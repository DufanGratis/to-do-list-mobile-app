const express = require("express")
const router = express.Router()
const { param, body, validationResult } = require("express-validator")
// const db = require("../dbconnector")
const db = require("../models/index")

router.get(
	"/",
	validate(),
	async (req, res) => {
		let dataCategories = await db.Categories.findAll()

		if (dataCategories.length !== 0) {
			return res.status(200).json(dataCategories)
		}
		else {
			return res.status(404).json({ errors: "There are no categories" })
		}	
	}
)

router.get(
	"/:id",
	param("id", "Categories ID must be an integer!").notEmpty().isNumeric(),
	validate(),
	async (req, res) => {
		let dataCategories = await db.Categories.findOne({
			where: { id: req.params.id }
		})
		
		if (dataCategories) {
			return res.status(200).json(dataCategories)
		}
		else {
			return res.status(404).json({ errors: `There is no category with id: ${ req.params.id }` })
		}
	}
)

router.get(
	"/:id/tasks",
	param("id", "Categories ID must be an integer!").notEmpty().isNumeric(),
	validate(),
	async (req, res) => {
		let dataTasks = await db.Tasks.findAll({
			where: { category: req.params.id }
		})
		if (dataTasks.length !== 0) {
			return res.status(200).json(dataTasks)
		}
		else {
			return res.status(404).json({ errors: `There are no tasks from the category with id: ${ req.params.id }` })
		}
	}
)

router.get(
	"/:id/total-tasks",
	param("id", "Categories ID must be an integer!").notEmpty().isNumeric(),
	validate(),
	async (req, res) => {
		let dataCategories = await db.Categories.findOne({
			where: { id: req.params.id }
		})
		let totalTasks = await db.Tasks.count({
			where: { category: req.params.id }
		})

		if (dataCategories) {
			return res.status(200).json(totalTasks)
		}
		else {
			return res.status(404).json({ errors: `There is no category with id: ${ req.params.id }` })
		}
	}
)

router.get(
	"/:id/tasks/:idt",
	param("id", "Categories ID must be an integer!").notEmpty().isNumeric(),
	param("idt", "Tasks ID must be an integer!").notEmpty().isNumeric(),
	validate(),
	async (req, res) => {
		let dataTasks = await db.Tasks.findOne({
			where: { id: req.params.idt, category: req.params.id }
		})
		
		if (dataTasks) {
			return res.status(200).json(dataTasks)
		}
		else {
			return res.status(404).json({ errors: `There is no task with id: ${ req.params.idt } from the category with id: ${ req.params.id }` })
		}
	}
)

router.post(
	"/",
	body("title", "Title must be filled with a string of minimum 3 and maximum 10 characters!").notEmpty().isString().isLength({ min: 3, max: 10 }),
	body("description", "Description must be filled with a string of minimum 6 and maximum 50 characters!").notEmpty().isString().isLength({ min: 6, max: 50 }),
	validate(),
	async (req, res) => {
		let newCategories = await db.Categories.create({
			title: req.body.title,
			description: req.body.description,
		})

		return res.status(200).send(`New category added with id: ${ newCategories.id }`)
	}
)

router.post(
	"/:id/tasks",
	param("id", "Categories ID must be an integer!").notEmpty().isNumeric(),
	body("title", "Title must be filled with a string of minimum 6 and maximum 20 characters!").notEmpty().isString().isLength({ min: 6, max: 20 }),
	body("description", "Description must be filled with a string of minimum 11 and maximum 300 characters!").notEmpty().isString().isLength({ min: 11, max: 300 }),
	body("deadline", "Deadline must be filled with a date of minimum today!").notEmpty().isDate().isAfter(new Date(new Date().setDate(new Date().getDate()-1)).toISOString()),
	validate(),
	async (req, res) => {
		let dataCategories = await db.Categories.findOne({
			where: { id: req.params.id }
		})

		if (dataCategories) {
			let newTasks = await db.Tasks.create({
				title: req.body.title,
				description: req.body.description,
				created: new Date().toISOString(),
				deadline: req.body.deadline,
				status: false,
				category: req.params.id,
			})
			return res.status(200).send(`New task added with id: ${ newTasks.id }`)
		}
		else {
			return res.status(404).json({ errors: `There is no category with id: ${ req.params.id }` })
		}
	}
)

router.put(
	"/:id/tasks/:idt",
	param("id", "Categories ID must be an integer!").notEmpty().isNumeric(),
	param("idt", "Tasks ID must be an integer!").notEmpty().isNumeric(),
	body("title", "Title must be filled with a string of minimum 6 and maximum 20 characters!").notEmpty().isString().isLength({ min: 6, max: 20 }),
	body("description", "Description must be filled with a string of minimum 11 and maximum 300 characters!").notEmpty().isString().isLength({ min: 11, max: 300 }),
	body("deadline", "Deadline must be filled with a date of minimum today!").notEmpty().isDate().isAfter(new Date(new Date().setDate(new Date().getDate()-1)).toISOString()),
	validate(),
	async (req, res) => {
		let category = req.params.id
		let id = req.params.idt
		let dataTasks = await db.Tasks.findOne({
			where: { id, category }
		})

		if (dataTasks) {
			dataTasks.title = req.body.title
			dataTasks.description = req.body.description
			dataTasks.deadline = req.body.deadline

			await dataTasks.save()
			
			return res.status(200).send(`Task with id: ${ id } has been edited.`)
		}
		else {
			return res.status(404).json({ errors: `There is no task with id: ${ req.params.idt } from the category with id: ${ req.params.id }` })
		}
	}
)

router.patch(
	"/:id/tasks/:idt",
	param("id", "Categories ID must be an integer!").notEmpty().isNumeric(),
	param("idt", "Tasks ID must be an integer!").notEmpty().isNumeric(),
	body("status", "Status must be filled with a boolean!").notEmpty().isBoolean(),
	validate(),
	async (req, res) => {
		let category = req.params.id
		let id = req.params.idt
		let dataTasks = await db.Tasks.findOne({
			where: { id, category }
		})

		if (dataTasks) {
			let status = dataTasks.status
			dataTasks.status = req.body.status

			await dataTasks.save()
			
			return res.status(200).send(`Task's status with id: ${ id } has been changed from ${ status } to ${ req.body.status }.`)
		}
		else {
			return res.status(404).json({ errors: `There is no task with id: ${ req.params.idt } from the category with id: ${ req.params.id }` })
		}
	}
)

router.delete(
	"/:id/tasks/:idt",
	param("id", "Categories ID must be an integer!").notEmpty().isNumeric(),
	param("idt", "Tasks ID must be an integer!").notEmpty().isNumeric(),
	validate(),
	async (req, res) => {
		let category = req.params.id
		let id = req.params.idt
		let dataTasks = await db.Tasks.findOne({
			where: { id, category }
		})

		if (dataTasks) {
			let deadline = dataTasks.deadline
			let time = new Date().toISOString()

			if (deadline.toISOString().slice(0, 10) > time.slice(0, 10)) {
				await db.Tasks.destroy({
					where: { id }
				})
				return res.status(200).send(`Task with id: ${ id } has been deleted.`)
			}
			else {
				return res.status(400).json({ errors: "Tasks' deadline must be at least tomorrow for deletion!" })
			}
		}
		else {
			return res.status(404).json({ errors: `There is no task with id: ${ req.params.idt } from the category with id: ${ req.params.id }` })
		}
	}
)

function validate() {
	return function (req, res, next) {
		const result = validationResult(req)
		if (!result.isEmpty()) {
			return res.status(400).json({ errors: result.array() })
		}
		else {
			next()
		}
	}
}

module.exports = router