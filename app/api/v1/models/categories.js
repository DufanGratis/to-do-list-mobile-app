const { DataTypes, Model } = require('sequelize')

module.exports = async (sequelize) => {

	class Categories extends Model {}

	Categories.init(
		{
			id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
			},
			title: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			description: {
				type: DataTypes.STRING,
				allowNull: false,
			}
  	},
		{
      sequelize,
      modelName: 'Categories',
      tableName: 'categories',
      defaultScope: {
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      },
    }
	)

  return Categories;
}