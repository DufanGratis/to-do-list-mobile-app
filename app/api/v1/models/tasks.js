const { DataTypes, Model } = require('sequelize')

module.exports = async (sequelize) => {

	class Tasks extends Model {}

	Tasks.init(
		{
			id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
			},
			title: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			description: {
				type: DataTypes.STRING,
				allowNull: false,
			},
			created: {
				type: DataTypes.DATE,
				allowNull: false,
			},
			deadline: {
				type: DataTypes.DATE,
				allowNull: false,
			},
			status: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
			},
			category: {
				type: DataTypes.INTEGER,
				allowNull: false,
				references: {
					model: "categories",
					key: "id",
				}
			}
  	},
		{
      sequelize,
      modelName: 'Tasks',
      tableName: 'tasks',
      defaultScope: {
        attributes: {
          exclude: ['createdAt', 'updatedAt'],
        },
      },
    }
	)

  return Tasks
}