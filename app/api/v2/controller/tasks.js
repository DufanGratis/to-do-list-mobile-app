const { param, body, validationResult } = require("express-validator")
const db = require("../models/index")

exports.findAll = (
	async (req, res, next) => {
		try {
			const result = validationResult(req)
			if (!result.isEmpty()) {
				return res.status(400).json({ errors: result.array() })
			}

			let dataTasks = await db.Tasks.findAll({
				where: { category_id: req.params.category_id }
			})
			if (dataTasks.length !== 0) {
				return res.status(200).json(dataTasks)
			}
			else {
				return res.status(404).json({ errors: `There are no tasks from the category with id: ${ req.params.category_id }` })
			}

		} catch (err) {
			return next(err)
		}
	}
)

exports.findOne = (
	async (req, res, next) => {
		try {
			const result = validationResult(req)
			if (!result.isEmpty()) {
				return res.status(400).json({ errors: result.array() })
			}

			let dataTasks = await db.Tasks.findOne({
				where: { id: req.params.task_id, category_id: req.params.category_id }
			})
			
			if (dataTasks) {
				return res.status(200).json(dataTasks)
			}
			else {
				return res.status(404).json({ errors: `There is no task with id: ${ req.params.task_id } from the category with id: ${ req.params.category_id }` })
			}

		} catch (err) {
			return next(err)
		}
	}
)

exports.create = (
	async (req, res, next) => {
		try {
			const result = validationResult(req)
			if (!result.isEmpty()) {
				return res.status(400).json({ errors: result.array() })
			}
			
			let dataCategories = await db.Categories.findOne({
				where: { id: req.body.category_id }
			})

			if (dataCategories) {
				let newTasks = await db.Tasks.create({
					title: req.body.title,
					description: req.body.description,
					created: new Date().toISOString(),
					deadline: req.body.deadline,
					status: false,
					category_id: req.body.category_id,
				})
				return res.status(200).send(`New task added with id: ${ newTasks.id }`)
			}
			else {
				return res.status(404).json({ errors: `There is no category with id: ${ req.body.category_id }` })
			}

		} catch (err) {
			return(err)
		}
	}
)

exports.update = (
	async (req, res, next) => {
		try {
			const result = validationResult(req)
			if (!result.isEmpty()) {
				return res.status(400).json({ errors: result.array() })
			}

			let id = req.body.id
			let category_id = req.body.category_id
			let dataTasks = await db.Tasks.findOne({
				where: { id, category_id }
			})

			if (dataTasks) {
				dataTasks.title = req.body.title
				dataTasks.description = req.body.description
				dataTasks.deadline = req.body.deadline

				await dataTasks.save()
				
				return res.status(200).send(`Task with id: ${ id } has been edited.`)
			}
			else {
				return res.status(404).json({ errors: `There is no task with id: ${ req.body.id } from the category with id: ${ req.body.category_id }` })
			}

		} catch (err) {
			return next(err)
		}
	}
)

exports.updateStatus = (
	async (req, res, next) => {
		try {
			const result = validationResult(req)
			if (!result.isEmpty()) {
				return res.status(400).json({ errors: result.array() })
			}

			let id = req.body.id
			let category_id = req.body.category_id
			let dataTasks = await db.Tasks.findOne({
				where: { id, category_id }
			})

			if (dataTasks) {
				let status = dataTasks.status
				dataTasks.status = req.body.status

				await dataTasks.save()
				
				return res.status(200).send(`Task's status with id: ${ id } has been changed from ${ status } to ${ req.body.status }.`)
			}
			else {
				return res.status(404).json({ errors: `There is no task with id: ${ req.body.id } from the category with id: ${ req.body.category_id }` })
			}

		} catch (err) {
			return next(err)
		}
	}
)

exports.delete = (
	async (req, res, next) => {
		try {
			const result = validationResult(req)
			if (!result.isEmpty()) {
				return res.status(400).json({ errors: result.array() })
			}

			let id = req.body.id
			let category_id = req.body.category_id
			let dataTasks = await db.Tasks.findOne({
				where: { id, category_id }
			})

			if (dataTasks) {
				let deadline = dataTasks.deadline
				let time = new Date().toISOString()

				if (deadline.toISOString().slice(0, 10) > time.slice(0, 10)) {
					await db.Tasks.destroy({
						where: { id }
					})
					return res.status(200).send(`Task with id: ${ id } has been deleted.`)
				}
				else {
					return res.status(400).json({ errors: "Tasks' deadline must be at least tomorrow for deletion!" })
				}
			}
			else {
				return res.status(404).json({ errors: `There is no task with id: ${ req.body.id } from the category with id: ${ req.body.category_id}` })
			}
		
		} catch (err) {
			return next(err)
		}
	}
)

exports.validate = (method) => {
	switch (method) {
		case "findAll": {
			return [
				param("category_id", "Categories ID must be filled!").notEmpty(),
				param("category_id", "Categories ID must be a number!").isNumeric(),
			]
		}
		case "findOne": {
			return [
				param("category_id", "Categories ID must be filled!").notEmpty(),
				param("category_id", "Categories ID must be a number!").isNumeric(),
				param("task_id", "Tasks ID must be filled!").notEmpty(),
				param("task_id", "Tasks ID must be a number!").isNumeric(),
			]
		}
		case "create": {
			return [
				body("title", "Title must be filled!").notEmpty(),
				body("title", "Title must be a string!").isString(),
				body("title", "Title must be at least 6 characters long!").isLength({ min: 6 }),
				body("title", "Title must be at most 20 characters long!").isLength({ max: 20 }),
				body("description", "Description must be filled!").notEmpty(),
				body("description", "Description must be a string!").isString(),
				body("description", "Description must be at least 11 characters long!").isLength({ min: 11 }),
				body("description", "Description must be at most 300 characters long!").isLength({ max: 300 }),
				body("deadline", "Deadline must be filled!").notEmpty(),
				body("deadline", "Deadline must be a valid date!").isDate(),
				body("deadline", "Deadline must be at least today!").isAfter(new Date(new Date().setDate(new Date().getDate()-1)).toISOString()),
				body("category_id", "Categories ID must be filled!").notEmpty(),
				body("category_id", "Categories ID must be a number!").isNumeric(),
			]
		}
		case "update": {
			return [
				body("id", "Tasks ID must be filled!").notEmpty(),
				body("id", "Tasks ID must be a number!").isNumeric(),
				body("title", "Title must be filled!").notEmpty(),
				body("title", "Title must be a string!").isString(),
				body("title", "Title must be at least 6 characters long!").isLength({ min: 6 }),
				body("title", "Title must be at most 20 characters long!").isLength({ max: 20 }),
				body("description", "Description must be filled!").notEmpty(),
				body("description", "Description must be a string!").isString(),
				body("description", "Description must be at least 11 characters long!").isLength({ min: 11 }),
				body("description", "Description must be at most 300 characters long!").isLength({ max: 300 }),
				body("deadline", "Deadline must be filled!").notEmpty(),
				body("deadline", "Deadline must be a valid date!").isDate(),
				body("deadline", "Deadline must be at least today!").isAfter(new Date(new Date().setDate(new Date().getDate()-1)).toISOString()),
				body("category_id", "Categories ID must be filled!").notEmpty(),
				body("category_id", "Categories ID must be a number!").isNumeric(),
			]
		}
		case "updateStatus": {
			return [
				body("id", "Tasks ID must be filled!").notEmpty(),
				body("id", "Tasks ID must be a number!").isNumeric(),
				body("category_id", "Categories ID must be filled!").notEmpty(),
				body("category_id", "Categories ID must be a number!").isNumeric(),
				body("status", "Status must be filled !").notEmpty(),
				body("status", "Status must be a boolean!").isBoolean(),
			]
		}
		case "delete": {
			return [
				body("id", "Tasks ID must be filled!").notEmpty(),
				body("id", "Tasks ID must be a number!").isNumeric(),
				body("category_id", "Categories ID must be filled!").notEmpty(),
				body("category_id", "Categories ID must be a number!").isNumeric(),
			]
		}
	}
}
