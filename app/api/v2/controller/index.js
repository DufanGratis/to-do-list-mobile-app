const categories = require("../controller/categories")
const tasks = require("../controller/tasks")

const controller = { categories, tasks }

module.exports = controller