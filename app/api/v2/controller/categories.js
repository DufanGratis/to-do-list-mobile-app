const { param, body, validationResult } = require("express-validator")
const db = require("../models/index")

exports.findAll = (
	async (req, res, next) => {
		try {
			const result = validationResult(req)
			if (!result.isEmpty()) {
				return res.status(400).json({ errors: result.array() })
			}

			let dataCategories = await db.Categories.findAll()

			if (dataCategories.length !== 0) {
				return res.status(200).json(dataCategories)
			}
			else {
				return res.status(404).json({ errors: "There are no categories" })
			}	

		} catch (err) {
			return next(err)
		}
	}
)

exports.findOne = (
	async (req, res, next) => {
		try {
			const result = validationResult(req)
			if (!result.isEmpty()) {
				return res.status(400).json({ errors: result.array() })
			}

			let dataCategories = await db.Categories.findOne({
				where: { id: req.params.category_id }
			})
			
			if (dataCategories) {
				return res.status(200).json(dataCategories)
			}
			else {
				return res.status(404).json({ errors: `There is no category with id: ${ req.params.id }` })
			}

		} catch (err) {
			return next(err)
		}
	}
)

exports.countTasks = (
	async (req, res, next) => {
		try {
			const result = validationResult(req)
			if (!result.isEmpty()) {
				return res.status(400).json({ errors: result.array() })
			}

			let dataCategories = await db.Categories.findOne({
				where: { id: req.params.category_id }
			})
			let totalTasks = await db.Tasks.count({
				where: { category_id: req.params.category_id }
			})

			if (dataCategories) {
				return res.status(200).json(totalTasks)
			}
			else {
				return res.status(404).json({ errors: `There is no category with id: ${ req.params.category_id }` })
			}

		} catch (err) {
			return next(err)
		}
	}
)

exports.create = (
	async (req, res, next) => {
		try {
			const result = validationResult(req)
			if (!result.isEmpty()) {
				return res.status(400).json({ errors: result.array() })
			}
			
			let newCategories = await db.Categories.create({
				title: req.body.title,
				description: req.body.description,
			})

			return res.status(200).send(`New category added with id: ${ newCategories.id }`)
	
		} catch (err) {
			return next(err)
		}
	}
)

exports.validate = (method) => {
	switch (method) {
		case "findAll": {
			return []
		}
		case "findOne": {
			return [
				param("category_id", "Categories ID must be filled!").notEmpty(),
				param("category_id", "Categories ID must be a number!").isNumeric(),
			]
		}
		case "countTasks": {
			return [
				param("category_id", "Categories ID must be filled!").notEmpty(),
				param("category_id", "Categories ID must be a number!").isNumeric(),
			]
		}
		case "create": {
			return [
				body("title", "Title must be filled!").notEmpty(),
				body("title", "Title must be a string!").isString(),
				body("title", "Title must be at least 3 characters long!").isLength({ min: 3 }),
				body("title", "Title must be at most 10 characters long!").isLength({ max: 10 }),
				body("description", "Description must be filled!").notEmpty(),
				body("description", "Description must be a string!").isString(),
				body("description", "Description must be at least 6 characters long!").isLength({ min: 6 }),
				body("description", "Description must be at most 50 characters long!").isLength({ max: 50 }),
			]
		}
	}
}
