const express = require("express")
const router = express.Router()
const controller = require("../controller/index")

router.get(
	"/",
	controller.categories.validate("findAll"),
	controller.categories.findAll,
)

router.get(
	"/:category_id",
	controller.categories.validate("findOne"),
	controller.categories.findOne,
)

router.get(
	"/:category_id/total-tasks",
	controller.categories.validate("countTasks"),
	controller.categories.countTasks,
)

router.post(
	"/",
	controller.categories.validate("create"),
	controller.categories.create,
)

module.exports = router