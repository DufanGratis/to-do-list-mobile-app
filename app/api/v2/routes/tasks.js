const express = require("express")
const router = express.Router()
const controller = require("../controller/index")

router.get(
	"/:category_id/tasks",
	controller.tasks.validate("findAll"),
	controller.tasks.findAll,
)

router.get(
	"/:category_id/tasks/:task_id",
	controller.tasks.validate("findOne"),
	controller.tasks.findOne
)

router.post(
	"/tasks",
	controller.tasks.validate("create"),
	controller.tasks.create,
)

router.put(
	"/tasks",
	controller.tasks.validate("update"),
	controller.tasks.update,
)

router.patch(
	"/tasks",
	controller.tasks.validate("updateStatus"),
	controller.tasks.updateStatus,
)

router.delete(
	"/tasks",
	controller.tasks.validate("delete"),
	controller.tasks.delete,
)

module.exports = router