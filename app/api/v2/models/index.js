const { Sequelize } = require("sequelize")

const db = {}

async function init() {
  db.sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USER,
    process.env.DB_PASS,
    {
      host: process.env.DB_HOST,
      dialect: "mysql",
      logging: false,
    }
  )

  await db.sequelize.authenticate()

  let files = require("fs").readdirSync(
    require("path").join(__dirname, "")
  )
  for (let i = 0; i < files.length; i++) {
    const model = require("./" + files[i])
    if (typeof model === "function") {
      await model(db.sequelize)
    }
  }

  await db.sequelize.sync()

  Object.assign(db, db.sequelize.models)

  console.log("Database connection (v2) has been established successfully.")
}
init()

module.exports = db
