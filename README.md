# API for To Do List Mobile Application
An API capable of getting, adding, editing, and deleting categories and tasks, written in Javascript Framework (NodeJS and ExpressJS) using MySQL database implementing REST API

## How to Run
* Create a local empty database
* Create .env file from .env.example and change the file with the appropriate data
* Run `node app.js`

## Requirements

* As a user I want to be able to get list category of task in home page
	* Retrieve all categories of task on home page
	* I can see Title of category and Number of task on there
* As a user I want to be able to create the category
	* I can input a Title and Description of category
	* Title must be less than or equal to 10 characters and more than 2 characters
	* Description must be less than or equal to 50 characters and more than 5 characters
* As a user I want to be able to get list tasks by category
	* Retrieve all tasks based on category
	* I can see Title of task
	* I can see Status of the task, so the task is done will be marking and checked
* As a user I want to be able to create a task
	* I can input a Title and Description of task and set the deadline date
	* Title must be less than or equal to 20 characters and more than 5 characters
	* Description must be less than or equal to 300 characters and more than 10 characters
	* The date of deadline must be more than or equal to current date
* As a user I want to be able to edit the task
* As a user I want to be able to delete the task
	* Deletion must be more than current date

## Database

* Categories table

Field       | Type         | Null | Key | Default | Extra          
----------- | ------------ | ---- | --- | ------- | -----
id          | int          | NO   | PRI | NULL    | auto_increment
title       | varchar(255) | NO   |     | NULL    |               
description | varchar(255) | NO   |     | NULL    |               


* Tasks table

Field       | Type         | Null | Key | Default | Extra          
----------- | ------------ | ---- | --- | ------- | -----
id          | int          | NO   | PRI | NULL    | auto_increment
title       | varchar(255) | NO   |     | NULL    |               
description | varchar(255) | NO   |     | NULL    |               
created     | datetime     | NO   |     | NULL    |               
deadline    | datetime     | NO   |     | NULL    |               
status      | tinyint(1)   | NO   |     | NULL    |               
category_id | int          | NO   | MUL | NULL    |               

## Methods

### GET

* `GET /api/v2/categories/`

	Return all categories from database

* `GET /api/v2/categories/category_id/`

	Return the category with id: category_id from database
		
* `GET /api/v2/categories/category_id/tasks/`

	Return all tasks from the category with id: category_id from database
		
* `GET /api/v2/categories/category_id/total-tasks/`

	Return the total number of tasks from the category with id: category_id from database

* `GET /api/v2/categories/category_id/tasks/task_id/`

	Return the task with id: task_id from the category with id: category_id from database

### POST

* `POST /api/v2/categories/`

	Create a new category consists of "title" and "description" to database
		
* `POST /api/v2/categories/tasks/`

	Create a new task consists of "title", "description", "deadline", and "category_id" to database

### PUT

* `PUT /api/v2/categories/tasks/`

	Edit the task consists of "id", "title", "description", "deadline", and "category_id" from database

### PATCH

* `PATCH /api/v2/categories/tasks`

	Edit the task consists of "id", "status", and "category_id" from database

### DELETE

* `DELETE /api/v2/categories/tasks`

	Delete the task consists of "id" and "category_id" from database
	