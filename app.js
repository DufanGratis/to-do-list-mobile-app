const express = require("express");
const app = express();

require("dotenv").config({ path: "./.env" })

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// require("./app/api/v1/models/index")
require("./app/api/v2/models/index")

// app.use("/api/v1", require("./app/api/v1/routes/index"))
app.use("/api/v2", require("./app/api/v2/routes/index"))

const PORT = 3000
app.listen(PORT, () => {
  console.log(`Server is running on port https://localhost:${PORT}.`);
});